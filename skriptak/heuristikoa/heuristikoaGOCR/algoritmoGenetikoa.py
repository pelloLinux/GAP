import random
from random import shuffle
import sys
import shlex, subprocess
import time
from time import sleep
import operator


#Algotimo genetikoa, irudiak aurreprozesatze egoki bat aurkitzeko, ondoren OCR egiteko
#Indibiduo bakoitza irudi eraldaketen lista bat izango da eta eraldaketa bat nahi adina aldiz agertzea onartuko da.
#9 eraldaketa desberdin erabiliko dira:
# 	null (ezer ez aplikatzea)
#	MORAN 1
#	MORAN 2 (Yosuren indizeak diren eraldaketei, "_" bat jarriko diegu aurretik, komandoa desberdina baita)
#	MORAN 3
#	Negate
#	Edge 2
#	Edge 4
#	Edge 6
#	Edge 8
#	Contrast


class indibiduo:
	#objetua sortu
	def __init__(self, eral, ebal):
		self.eraldaketak = eral
		self.ebaluazioa = ebal
 
 
 
def hasierakoPopulazioa(eraldaketak):

	#eraldaketa denak denekin 10^2 indibiduo sortuko ditugu alde batetik
	indibiduoak = []

	for e1 in eraldaketak:
		for e2 in eraldaketak:
			eral = [e1,e2,"null","null","null","null","null","null"]
			inb = indibiduo(eral,-1)#oraindik ebaluatu gabe baitago
			indibiduoak.append(inb)

 
	# beste n_indibiduo guztiz ausaz
	for i in range(n_ausaz):
		eral = []
		for i in range(8):
			r = random.randrange(0, len(eraldaketak))
			eral.append(eraldaketak[r])
		inb = indibiduo(eral,-1)#oraindik ebaluatu gabe baitago
		indibiduoak.append(inb)

	return indibiduoak	
		

#Erdiak batetik eta beste erdiak bestetik
def elekzioaLehena(indibiduoak):

	kop = len(eraldaketak)*len(eraldaketak)
	lag1 = []
	lag2 = []

	print(len(indibiduoak))

	#sortutako moduaren arabera sailkatu
	lag1 = indibiduoak[0:kop]
	lag2 = indibiduoak[kop:len(indibiduoak)]

	#ordenatu
	lag1.sort(key=operator.attrgetter('ebaluazioa'),reverse = True)
	lag2.sort(key=operator.attrgetter('ebaluazioa'),reverse = True)

	#sailkapen bakoitzetik indibiduen erdiak hartu
	lag1 = lag1[0:int(n_indibiduaoak/2)]
	lag2 = lag2[0:int(n_indibiduaoak/2)]

	return lag1 + lag2
 


def elekzioa(indibiduoak):

	denera = 0
	gehikuntzak = []
	gurasoak = []
	aurrekoa = 0
	for ind in indibiduoak:
		denera += ind.ebaluazioa
		gehikuntzak.append(denera)

	#gurasoak hautatu(roulete wheel selection)
	for i in range(int(3/4*n_indibiduaoak)): #3/4 guraso
		#ausazko zenbakia lortu
		r = random.randrange(0, int(denera))
		#ausazko zenbakia zein indizeri dagokion aurkitu
		for j in range(len(indibiduoak)):
			if r < gehikuntzak[j]:
				gurasoak.append(indibiduoak[j])
				break
	return gurasoak			


def irudiakEraldatu(indibiduoak):
 
	komandoa = ""
	#indibiduo bakoitzeako
	for i in range(len(indibiduoak)):
		print (str(i)+". indibiduoa")
		ind = indibiduoak[i];
		print(ind)
		for j in range(len(ind.eraldaketak)):
			eral = ind.eraldaketak[j]
			#yosuren indizeen kasuan izena aldatzeko
			komandoa2 = ""
			
						#lehena bada
			if j==0:
				#yosuren indizeak bada
				if eral[0] == '_':
					zer = eral[2]
					num = eral[1]
					komandoa = "./demo "+argazkia+" "+num+" "+zer
					komandoa2 = "mv thres.png ir"+str(j)
				#edge bada
				elif eral[2] == 'd':
					num = eral[0]
					komandoa = "convert -edge "+num+" "+argazkia+" ir"+str(j)
				#null bada
				elif eral[1]=='u':
					komandoa = "convert "+argazkia+" ir"+str(j)
				#normalize bada
				elif eral[2]=='r':
					komandoa = "convert -normalize "+argazkia+" ir"+str(j)
				#negate bada
				elif eral[0]=='n':
					komandoa = "convert -negate "+argazkia+" ir"+str(j)
				#kontarst bada
				elif eral[0]=='c':
					komandoa = "convert -contrast "+argazkia+" ir"+str(j)

			#azkena bada
			elif j==len(ind.eraldaketak)-1:
				#yosuren indizeak bada
				if eral[0] == '_':
					zer = eral[2]
					num = eral[1]
					komandoa = "./demo ir"+str(j-1)+" "+num+" "+zer
					komandoa2 = "mv thres.png indibi"+str(i)
				#edge bada
				elif eral[2] == 'd':
					num = eral[0]
					komandoa = "convert -edge "+num+" ir"+str(j-1)+" indibi"+str(i)+".jpg"
				#null bada
				elif eral[1]=='u':
					komandoa = "convert ir"+str(j-1)+" indibi"+str(i)+".jpg"
				#normalize bada
				elif eral[2]=='r':
					komandoa = "convert -normalize ir"+str(j-1)+" indibi"+str(i)+".jpg"
				#negate bada
				elif eral[0]=='n':
					komandoa = "convert -negate ir"+str(j-1)+" indibi"+str(i)+".jpg"
				#kontarst bada
				elif eral[0]=='c':
					komandoa = "convert -contrast ir"+str(j-1)+" indibi"+str(i)+".jpg"
			

			else:
			#yosuren indizeak bada
				if eral[0] == '_':
					zer = eral[2]
					num = eral[1]
					komandoa = "./demo ir"+str(j-1)+" "+num+" "+zer
					komandoa2 =  "mv thres.png ir"+str(j)
				#edge bada
				elif eral[2] == 'd':
					num = eral[0]
					komandoa = "convert -edge "+num+" ir"+str(j-1)+" ir"+str(j)
				#null bada
				elif eral[1]=='u':
					komandoa = "convert ir"+str(j-1)+" ir"+str(j)
				#normalize bada
				elif eral[2]=='r':
					komandoa = "convert -normalize ir"+str(j-1)+" ir"+str(j)
				#negate bada
				elif eral[0]=='n':
					komandoa = "convert -negate ir"+str(j-1)+" ir"+str(j)
				#kontarst bada
				elif eral[0]=='c':
					komandoa = "convert -contrast ir"+str(j-1)+" ir"+str(j)
			
			#komandoa exekutatu
			print(komandoa)
			args = shlex.split(komandoa)
			subprocess.call(args)
			#yosuren indizea bada
			if(komandoa2!=""):
				args = shlex.split(komandoa2)
				subprocess.call(args)

			
def irudiakEbaluatu(indibiduoak):
	
	#indibiduo guztiak ebaluatu
	for i in range(len(indibiduoak)):
		#komandoa = "tesseract -l eng indibi"+str(i)+" indibi"+str(i)
		komandoa = "gocr -i indibi"+str(i)+".jpg -o indibi"+str(i)+".txt"
		print(komandoa)
		args = shlex.split(komandoa)
		subprocess.call(args)

		#konparatu
		ehunekoa = testuakKonparatu("indibi"+str(i)+".txt", textOCR)
		indibiduoak[i].ebaluazioa = ehunekoa

	return indibiduoak	



def testuakKonparatu(izena1, izena2):

	#lehenengo fitxategia irakurri
	fitx1 = open(izena1,'r')
	list1 = []
	for line in fitx1:
		l =  line.split(" ")
		for i in range(0,len(l)):
			string = l[i]
			string.replace(",","")
			string.replace(".","")
			string.replace("\n","")
			if string != "\n":
				list1.append(string)
	fitx1.close()

	#bigarren fitxategia irakurri
	fitx2 = open(izena2,'r')
	list2 = []
	for line in fitx2:
		l =  line.split(" ")
		for i in range(0,len(l)):
			string = l[i]
			string.replace(",","")
			string.replace(".","")
			if string != "\n":
				list2.append(string)

	fitx2.close()



	kont = 0
	#Bi listen arteko antzekotasunak begiratu
	for i in range(0,len(list1)):
		 for j in range(0,len(list2)):
		 	if list1[i]!="" and list2[j]!="":
		 		if list1[i] == list2[j]:
		 			kont = kont + 1
		 			list1[i]=""
		 			list2[j]=""
		 			break

	#for i in range(0,len(list1)):
	#	if list1[i] != "" and list1[i] != "\n":
	#		print(list1[i])


	#for i in range(0,len(list2)):
	#	if list2[i] != "" and list2[i] !="\n":
	#		print(list2[i])


	if len(list1)!=0:
		ehunekoa = kont * 100 / len(list2)
	else:
		ehunekoa = 0
		
	print("**************************************************************")
	print("1 fixategiak bigarrenarekin duen antza => %"+str(ehunekoa))
	print("**************************************************************\n")

	return ehunekoa
	#outfile = open(sys.argv[3], 'a')
	#outfile.write(sys.argv[1] + " => %"+str(ehunekoa)+'\n')

#guraso kopuruak bikoitia izan behar du
def gurutzaketak(gurasoak):

	berriak = []
	print(len(gurasoak)/2)
	for i in range(int(len(gurasoak)/2)):
		#print("indizea "+str(i)+" => "+str(indizeak[i]))
		gur1 = gurasoak[i]
		gur2 = gurasoak[i+1]
		#gurutzatu egingo ditugu(one point crossover)
		r = random.randrange(1, 101)
		#%90-eko probabilitatearekin egingo dugu gurutzaketa
		if(r > 10):
			#gurutzaketa zein puntutan egingo den erabaki
			r_ind = random.randrange(8)
			berria1 = gur1.eraldaketak[0:(r_ind)] + gur2.eraldaketak[r_ind:8]
			berria2 = gur2.eraldaketak[0:(r_ind)] + gur1.eraldaketak[r_ind:8]
			indBer1 = indibiduo(berria1,-1)
			indBer2 = indibiduo(berria2,-1)
			berriak.append(indBer1)
			berriak.append(indBer2)
		else:
			berriak.append(gur1)
			berriak.append(gur2)

	return berriak				




def mutazioa(berriak): 
	
	mutatuakEdoEz = []
	for berri in berriak:
		r = random.randrange(1, 101)
		#%10-eko probabilitatearekin egingo dugu gurutzaketa
		if(r > 90):
			non_ind = random.randrange(8)
			zer_ind = random.randrange(len(eraldaketak))
			berri.eraldaketak[non_ind] = eraldaketak[zer_ind]
			mutatuakEdoEz.append(berri)
		else:
			mutatuakEdoEz.append(berri)

	return mutatuakEdoEz

def populazioBerria(indibiduoak, berriak):
	
	#berriak aurreko populazioko 1/4 nekin nahastuta
	indibiduoak.sort(key=operator.attrgetter('ebaluazioa'),reverse = True)
	lag = indibiduoak[0:int(n_indibiduaoak/4)]
	populazioBerria = berriak + lag
	return populazioBerria

 
 

print("*** ALGORITMO GENETIKOA ***")
print("")


#eraldaketa mota guztiak 
eraldaketak=["null","_1M","_2M","_3M","negate","2edge","4edge","6edge","8edge","contrast","normalize"]
#eraldaketak=["null","_1M","_2M","_3M","_1G","_2G","_3G","_1L","_2L","_3L","negate","2edge","4edge","6edge","8edge","contrast","normalize"]
#eraldaketak=["null","negate","2edge","contrast"]
#indibiduo bakoitzak 8 eraldaketa izango ditu
indibiduoak=[]
#populazio berriak sortzeko gurasoak
gurasoak=[]
#eraldatuko dugun argazki originalaren path-a
argazkia = sys.argv[1]
#OCR-egitean fallo kopurua begiratzeko eskuz idatzitako testua
textOCR = sys.argv[2]
#eraldaketen konbinaziorik onena gordeko da
onenaList = []
onenaEbal = 0

 
n_indibiduaoak = 50#int(input("Zenbat indibiduo ?? ->"))
n_ausaz = 100#int(input("Zenbat ausaz ?? ->"))
n_generazioak = 20#int(input("Zenbat generazio ?? ->"))
indibiduoak = hasierakoPopulazioa(eraldaketak)
irudiakEraldatu(indibiduoak)
indibiduoak = irudiakEbaluatu(indibiduoak)
indibiduoak = elekzioaLehena(indibiduoak)


for generazio in range(n_generazioak):
    print(str(generazio) + ". Generazioa")

    irudiakEraldatu(indibiduoak)
    indibiduoak = irudiakEbaluatu(indibiduoak)   


    #orain arteko onena hobetu badu
    for i in range(len(indibiduoak)):
    	if(indibiduoak[i].ebaluazioa>onenaEbal):
    		onenaEbal = indibiduoak[i].ebaluazioa
    		onenaList = indibiduoak[i].eraldaketak

    print("****************************************")
    print("****************************************")

    print(onenaList)
    print(onenaEbal)
    print("****************************************")
    for ind in indibiduoak:
    	print(str(ind.ebaluazioa)+"  "+str(ind.eraldaketak))
    print("****************************************")
    print("****************************************")
    sleep(2)

    gurasoak = elekzioa(indibiduoak) 
    berriak = gurutzaketak(gurasoak)
    indibiduoak = mutazioa(berriak)
    #indibiduoetatik 3/4 berriak sortu dira
    #1/4 berriz, lehengo populazioko onenak izango dira
    indibiduoak = populazioBerria(indibiduoak, berriak)

    outfile = open(argazkia+"emaitzak", 'a')
    outfile.write(str(ind.ebaluazioa))
    outfile.write(str(ind.eraldaketak))










 
