#!/bin/sh


if [ $# -lt 4 ]; then
	echo "Hiru argumentu behar ditu: ArgazkiOriginala OCReginda.txt Hizkuntza testuakGordetzeko"
	exit 1
fi

#irudiei ertzak bilatu modu desberdinetan
#sh irudiakEraldatu.sh $1

mkdir $4

#originalari OCR egin
#tesseract -l $3 $1 $1
#konparaketa egin
#python konparatu.py "$1.txt" $2 "emaitzak$1.txt"
#mv "$1.txt" $4

#eraldatutako irudi bakoitzeko
for i in $(ls irudiak)
do
	#tesseract -l $3 irudiak/$i $i
	gocr -i irudiak/$i -o gorc$i.txt
	#python konparatu.py "$i.txt" $2 "emaitzak$1.txt"
	python konparatu.py gorc$i.txt $2 emaitzakGocr$1.txt
	#mv "$i.txt" $4
	mv gocr$i.txt $4
done 

#rm irudiak/*





