#!/bin/sh


if [ $# -lt 1 ]; then
	echo "Argumentu bat behar du: Irudia"
	exit 1
fi


#irudia modu desberdinetara eraldatuko dugu
#MORAN
./demo $1 1 M
mv thres.png "irudiak/$1_1M.png"

./demo $1 2 M
mv thres.png "irudiak/$1_2M.png"

./demo $1 3 M
mv thres.png "irudiak/$1_3M.png"

#GEARY

./demo $1 1 G
mv thres.png "irudiak/$1_1G.png"

./demo $1 2 G
mv thres.png "irudiak/$1_2G.png"

./demo $1 3 G
mv thres.png "irudiak/$1_3G.png"

#LEBART

./demo $1 1 L
mv thres.png "irudiak/$1_1L.png"

./demo $1 2 L
mv thres.png "irudiak/$1_2L.png"

./demo $1 3 L
mv thres.png "irudiak/$1_3L.png"

#EDU

./demo $1 1 E2
mv thres.png "irudiak/$1_1E.png"

./demo $1 2 E2
mv thres.png "irudiak/$1_2E.png"

./demo $1 3 E2
mv thres.png "irudiak/$1_3E.png"

#EDU2

./demo $1 1 E1
mv thres.png "irudiak/$1_1E1.png"

./demo $1 2 E1
mv thres.png "irudiak/$1_2E1.png"

./demo $1 3 E1
mv thres.png "irudiak/$1_3E1.png"


