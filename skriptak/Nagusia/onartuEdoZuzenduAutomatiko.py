
import codecs
import io
import sys
import shlex, subprocess
from PIL import Image
import psutil


###############
###HASIERA#####
###############


#fitxategiak ireki
zuzenduak = io.open("mySemaitzaHand.txt")#, encoding="ISO-8859-1")
textuaOcr = open("handwrt1.txt","r")#ez kargatu guztiz ondo dagoena, OCR egindakoa baizik
hitzenLista = open("checkme.lst","r")
#zuzendua idazteko
outfile = open("zuzenduta.txt", 'a')

gaizkiHitza = ""
zuzentzekoAukerak = []
zuzen = True

zuzentzekoIlara = textuaOcr.readline()

#hiztegirekin konparatutakoa prozesatu
for line in zuzenduak:
	if "is okay" in line:
		gaizkiHitza = line.split("\"")[1].split("\"")[0]
		print("+++++++++++++++++++++++++++++++++++++++")
		print(str(gaizkiHitza)+" +++++ Ondo")
		print("+++++++++++++++++++++++++++++++++++++++\n")
		zuzen = True


	if "is incorrect" in line:
		gaizkiHitza = line.split("\"")[1].split("\"")[0]
		print("---------------------------------------")
		print(str(gaizkiHitza)+" ----- Gaizki")
		print("---------------------------------------\n")
		zuzentzekoAukerak = []
		zuzen = False

	if "..." in line:
		aukera = line.split("\"")[1].split("\"")[0]
		zuzentzekoAukerak.append(aukera)

	if "\n" == line:
		#hitza irakurri eta ilara jauzia endu
		momentukoHitza = hitzenLista.readline()
		momentukoHitza = momentukoHitza.replace("\n","")

		if momentukoHitza in zuzentzekoIlara:
			if zuzen == False:
				#zuzentzeko aukera badagoela ziurtatu
				if len(zuzentzekoAukerak)>0:
					print("** ZUZENKETA **      "+str(gaizkiHitza)+" => "+str(zuzentzekoAukerak[0]))
					#ilara zuzendu
					zuzentzekoIlara = zuzentzekoIlara.replace(momentukoHitza,zuzentzekoAukerak[0])
		else:
			#zuzendutako ilara idatzi
			outfile.write(zuzentzekoIlara)
			zuzentzekoIlara = textuaOcr.readline()
			if zuzen == False:
				if len(zuzentzekoAukerak)>0:
					print("** ZUZENKETA **      "+str(gaizkiHitza)+" => "+str(zuzentzekoAukerak[0]))
					#ilara zuzendu
					zuzentzekoIlara = zuzentzekoIlara.replace(momentukoHitza,zuzentzekoAukerak[0])
#azkenekoa ere idatzi behar da			
outfile.write(zuzentzekoIlara)




