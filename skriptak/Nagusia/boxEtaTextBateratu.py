import sys
import shlex, subprocess

class hitza:
	#objetua sortu
	def __init__(self, num):
		self.num = num
		self.minLeft = 10000000
		self.maxRight = -1
		self.minTop = 10000000
		self.maxBotton = -1
		self.xDim = -1
		self.yDim = -1
		self.xPos = -1
		self.yPos = -1
		self.hitz = ""

 
#kargatuko diren fitxategiak kargatu
#zuzenduak = open("checkme.lst")
boxak = open(sys.argv[1], 'r')

#zuzenduak-en tesseract-ek beste bere zuzentzailea pasatzen dionez,
#letra kopurua ez da berdina, bakoitzaren hasierako indizea bilatu

hitzak = [] #box bakoitzaren
aurrekoPos = int(-1)
#hasierako hitza sortu
num = 0
h = hitza(num)

for box in boxak:

	#box-aren balioak lortu
	letra = box.split(" ")[0]
	leftPos = int(box.split(" ")[1])
	rightPos = int(box.split(" ")[3])
	bottonPos = int(box.split(" ")[4])
	topPos = int(box.split(" ")[2])

	if abs(aurrekoPos - leftPos) > 10:#balio hori agiant entrenatu beharko da
		#hitz berria da
		print(h.hitz)
		print(h.minLeft)
		print(h.maxBotton)
		print(h.maxRight)
		print(h.minTop)

		h.xDim = int(abs(int(h.minLeft)-int(h.maxRight))) 
		h.yDim = int(abs(int(h.minTop)-int(h.maxBotton)))

		h.xPos = int(h.minLeft)
		h.yPos = int(326-h.maxBotton)#identify -format "%wx%h" handwrt1.jpg

		komandoa = "convert -crop "+str(h.xDim)+"x"+str(h.yDim)+"+"+str(h.xPos)+"+"+str(h.yPos)+" handwrt1.jpg abe.tiff"#izenak globalizatu behar dira
		args = shlex.split(komandoa)
		#subprocess.call(args)
		print(komandoa)
		print("\n")
		
		hitzak.append(h)

		#hitz berria sortu
		num += 1
		h = hitza(num)
		h.hitz = letra
	else:
		h.hitz += letra

	# box-aren balioak lortu
	letra = box.split(" ")[0]
	leftPos = int(box.split(" ")[1])
	rightPos = int(box.split(" ")[3])
	bottonPos = int(box.split(" ")[4])
	topPos = int(box.split(" ")[2])

	#hitza eguneratu
	if h.minLeft > leftPos:
		h.minLeft = leftPos
	if h.maxRight < rightPos:
		h.maxRight = rightPos
	if h.minTop > topPos:
		h.minTop = topPos
	if h.maxBotton < bottonPos:
		h.maxBotton = bottonPos

	aurrekoPos = rightPos
	