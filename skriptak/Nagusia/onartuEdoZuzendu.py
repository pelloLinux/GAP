
import codecs
import io
import sys
import shlex, subprocess
from PIL import Image
import psutil



class hitza:
	#objetua sortu
	def __init__(self, num):
		self.num = num
		self.minLeft = 10000000
		self.maxRight = -1
		self.minTop = 10000000
		self.maxBotton = -1
		self.xDim = -1
		self.yDim = -1
		self.xPos = -1
		self.yPos = -1
		self.hitz = ""

#teknika probisionala
def lortuIndBoxHitza(hitza, boxHitzak, ind):
	#box-eko hitza eta berezko hitzaren artean antzekotasuna bilatu
	
	#hasierako agorsetzeko
	indLag = ind

	while(True):
		# gutxienez %50-a berdina izan behar, berdinak direla eateko
		berdinak = 0
		for i in range(len(hitza)):
			for j in range(len(boxHitzak[ind].hitz)):
				if hitza[i]==boxHitzak[ind].hitz[j]:
					berdinak += 1
					break

		print("[Konparazioa] "+str(hitza)+" == "+str(boxHitzak[ind].hitz)+" => "+str(berdinak))
		if float(berdinak) > float(len(hitza)/3):
			#hitz hau dela suposatzen da
			indBerria = ind
			return indBerria
		else:
			ind += 1
			if ind > indLag + 10:
				#ez du aurkitu eta ez da irudia azalduko
				return -1




def boxEtaTextBateratu(boxFitxIzena,irudiIzena):
	
	#kargatuko diren fitxategiak kargatu
	#zuzenduak = open("checkme.lst")
	boxak = open(boxFitxIzena, 'r')

	#zuzenduak-en tesseract-ek beste bere zuzentzailea pasatzen dionez,
	#letra kopurua ez da berdina, bakoitzaren hasierako indizea bilatu

	hitzak = [] #box bakoitzaren
	aurrekoPos = int(-1)
	#hasierako hitza sortu
	num = 0
	h = hitza(num)
	#irudiaren dimentsioak lortu
	komandoa = "sh dimLortu.sh "+str(irudiIzena)
	args = shlex.split(komandoa)
	subprocess.call(args)
	#fitxategitik dimentxioa lortu
	dim = open("dim.txt", 'r')
	for line in dim:
		dimY = int(line.split("x")[1].split("\"")[0])


	for box in boxak:

		#box-aren balioak lortu
		letra = box.split(" ")[0]
		leftPos = int(box.split(" ")[1])
		rightPos = int(box.split(" ")[3])
		bottonPos = int(box.split(" ")[4])
		topPos = int(box.split(" ")[2])

		if abs(aurrekoPos - leftPos) > 15:#balio hori agiant entrenatu beharko da
			#hitz berria da
			print(h.hitz)
			print(h.minLeft)
			print(h.maxBotton)
			print(h.maxRight)
			print(h.minTop)
			print("\n")

			h.xDim = int(abs(int(h.minLeft)-int(h.maxRight))) 
			h.yDim = int(abs(int(h.minTop)-int(h.maxBotton)))

			h.xPos = int(h.minLeft)
			h.yPos = int(dimY-h.maxBotton)

			if h.maxBotton != 10000000:
				hitzak.append(h)

			#hitz berria sortu
			num += 1
			h = hitza(num)
			h.hitz = letra
		else:
			h.hitz += letra

		# box-aren balioak lortu
		letra = box.split(" ")[0]
		leftPos = int(box.split(" ")[1])
		rightPos = int(box.split(" ")[3])
		bottonPos = int(box.split(" ")[4])
		topPos = int(box.split(" ")[2])

		#hitza eguneratu
		if h.minLeft > leftPos:
			h.minLeft = leftPos
		if h.maxRight < rightPos:
			h.maxRight = rightPos
		if h.minTop > topPos:
			h.minTop = topPos
		if h.maxBotton < bottonPos:
			h.maxBotton = bottonPos

		aurrekoPos = rightPos

	#hitzak boxetan itzuli
	return hitzak



###############
###HASIERA#####
###############


#fitxategiak ireki
zuzenduak = io.open("mySemaitzaHand.txt")#, encoding="ISO-8859-1")

#box-ak markatuta dauden fitxategia
boxHitzak = boxEtaTextBateratu("handwrt1.box","handwrt1.jpg")
boxInd = 0
#zein ilaratan gauden jakiteko lista hau erabili behar da
hitzenLista = open("checkme.lst","r")
textuaOcr = open("handwrt1.txt","r")#ez kargatu guztiz ondo dagoena, OCR egindakoa baizik
#zuzendua idazteko
outfile = open("zuzenduta.txt", 'a')

gaizkiHitza = ""
zuzentzekoAukerak = []
zuzen = True

#lehenengo ilara irakurri
zuzentzekoIlara = textuaOcr.readline()

#hiztegirekin konparatutakoa prozesatu
for line in zuzenduak:
	if "is okay" in line:
		print("+++++++++++++++++++++++++++++++++++++++")
		print(str(line.split("\"")[1].split("\"")[0])+" +++++ Ondo")
		print("+++++++++++++++++++++++++++++++++++++++\n")
		zuzen = True
		#box-eko indizea eguneratu
		boxInd += 1

	if "is incorrect" in line:
		gaizkiHitza = line.split("\"")[1].split("\"")[0]
		print("---------------------------------------")
		print(str(gaizkiHitza)+" ----- Gaizki")
		print("---------------------------------------\n")
		zuzentzekoAukerak = []
		zuzen = False

	if "..." in line:
		aukera = line.split("\"")[1].split("\"")[0]
		zuzentzekoAukerak.append(aukera)

	if "\n" == line:
		if zuzen == False:
			print("\t 0- [ondo dago]")
			for i in range(len(zuzentzekoAukerak)):
				print("\t "+str(i+1)+"- "+str(zuzentzekoAukerak[i]))
			print("\t "+str(len(zuzentzekoAukerak)+1)+"- [eskuz zuzendu]")

			#boxeko hitza eta zuzentzekoa bat datozen indizea bueltatu
			boxIndLag = boxInd # aurrekoa gordetzeko
			boxInd = lortuIndBoxHitza(gaizkiHitza, boxHitzak, boxInd)

			if boxInd != -1:

				#irudia ebaki
				#print("::::::::"+str(boxHitzak[boxInd].hitz))
				komandoa = "convert -crop "+str(boxHitzak[boxInd].xDim)+"x"+str(boxHitzak[boxInd].yDim)+"+"+str(boxHitzak[boxInd].xPos)+"+"+str(boxHitzak[boxInd].yPos)+" handwrt1.jpg abe.tiff"#izenak globalizatu behar dira
				args = shlex.split(komandoa)
				subprocess.call(args)
				#irudikatu
				image = Image.open('abe.tiff')
				image.show()

			else:
				print("^^^^^^ IRUDIAN EZ DA AURKITU, garrantsitsua bada eskuz begiratu ^^^^^^")
				boxInd = boxIndLag

			#erabiltzaileak nahi duena aukeratu
			while True:
				try:#zenbaki bat sartzen dugula ziurtatu
					aukera = int(input(""))
					if aukera >= 0 and aukera<=len(zuzentzekoAukerak)+1:
						break #zenbakia aukeren artean dagoela ziurtatu
					else:
						print("Aukeretan agertzen den zenbaki bat sartu!")

				except ValueError:
					print("Aukeretan agertzen den zenbaki bat sartu!")

			if aukera == 0:
				zuzendutakoHitza = gaizkiHitza
			elif aukera > 0 and aukera < (len(zuzentzekoAukerak)+1):
				zuzendutakoHitza = zuzentzekoAukerak[aukera-1]
			elif aukera == len(zuzentzekoAukerak)+1:
				zuzendutakoHitza = input("Idatzi hitza zuzenduta => ")


			#irudia pantallatik kendu
			for proc in psutil.process_iter():
				if proc.name() == "display":
					proc.kill()

			#indizea eguneratu
			boxInd += 1

		#hitza irakurri eta ilara jauzia kendu
		momentukoHitza = hitzenLista.readline()
		momentukoHitza = momentukoHitza.replace("\n","")

		if momentukoHitza in zuzentzekoIlara:
			if zuzen == False:
				print(str(momentukoHitza)+" => "+str(zuzendutakoHitza)+" :::: "+str(zuzentzekoIlara))
				zuzentzekoIlara = zuzentzekoIlara.replace(momentukoHitza,zuzendutakoHitza)
		else:
			#zuzendutako ilara idatzi
			outfile.write(zuzentzekoIlara)
			zuzentzekoIlara = textuaOcr.readline()
			#ilera hutsa bada
			while zuzentzekoIlara == "\n":#hau baldintza izan beharrean iterazioak
				outfile.write("\n")
				zuzentzekoIlara = textuaOcr.readline()
			if zuzen == False:
				print(str(momentukoHitza)+" => "+str(zuzendutakoHitza)+" :::: "+str(zuzentzekoIlara))
				#ilara zuzendu
				zuzentzekoIlara = zuzentzekoIlara.replace(momentukoHitza,zuzendutakoHitza)
		#print("      "+str(zuzentzekoIlara))

#azkenekoa ere idatzi behar da			
outfile.write(zuzentzekoIlara)			





