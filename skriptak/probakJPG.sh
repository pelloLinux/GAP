#!/bin/sh

#tesseract pasa eta emaitzak konparatu

if [ $# -lt 1 ]; then
	echo "Bi argumentu behar ditu: [sarreraOriginala OCRegindakoTestua]"
	exit 1
fi




#OCR egin
tesseract "$1" "ocrBerria"

#fitxategiak konparatu
python konparatu.py $1_ocr1.txt ocrBerria.txt

#behar ez diren fitxategiak ezabatu
rm $1_ocr1.txt ocrBerria.txt "tessetactEgiteko.tiff"



