from subprocess import call
import re, sys, string

#weka-ko algoritmoa exekutatu
komandoa = "java -cp /usr/share/java/weka.jar weka.classifiers.bayes.NaiveBayes -t proba.arff -T num1.arff  > erantzuna1.txt"
call(komandoa, shell=True)

#erantzuna gordetzeko
f = open("erantzuna1.txt", 'r')
fitxString = f.read()

split = fitxString.split("=== Confusion Matrix ===\n\n a b c   <-- classified as\n")[2].split("|")[0].split(" ")
split = split[1:-1]#lehenengoa eta azkena kentzeko
for i in range(0,3):
	if int(split[i])==1:
		call("echo "+str(i+1)+" > erantzunaNum1.txt", shell=True)#zenbakia fitxategian inprimatu

