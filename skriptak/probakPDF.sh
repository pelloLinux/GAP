#!/bin/sh

#tesseract pasa eta emaitzak konparatu

if [ $# -lt 1 ]; then
	echo "Bi argumentu behar ditu: [sarreraOriginalaOCRegindakoa]"
	exit 1
fi

#OCR eginiko informazioa .txt batera pasa
pdftotext $1 "$1_ocr1.txt"

#fitxategia OCR egiteko prestatu
sh pdfTesseract.sh $1 "tessetactEgiteko"

#OCR egin
tesseract "tessetactEgiteko.tiff" "ocrBerria"

#fitxategiak konparatu
python konparatu.py $1_ocr1.txt ocrBerria.txt

#behar ez diren fitxategiak ezabatu
rm $1_ocr1.txt ocrBerria.txt "tessetactEgiteko.tiff"



