\select@language {basque}
\contentsline {chapter}{Laburpena}{i}{chapter*.1}
\contentsline {chapter}{Gaien aurkibidea}{iii}{chapter*.2}
\contentsline {chapter}{Irudien aurkibidea}{vii}{chapter*.3}
\contentsline {chapter}{Taulen aurkibidea}{xi}{chapter*.4}
\contentsline {chapter}{\numberline {1}Sarrera}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motibazioa}{2}{section.1.1}
\contentsline {chapter}{\numberline {2}Aurrekariak eta Teknologia}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Irudien Tratamendua}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Irudien Transformazioak}{4}{subsection.2.1.1}
\contentsline {subsubsection}{Kontrastea Aldatzea}{4}{subsubsection*.5}
\contentsline {subsubsection}{Normalizazioa}{4}{subsubsection*.6}
\contentsline {subsubsection}{Negate}{5}{subsubsection*.7}
\contentsline {subsubsection}{Ertzen Detekzioa}{5}{subsubsection*.8}
\contentsline {subsubsection}{Gaussian-Blur}{6}{subsubsection*.9}
\contentsline {subsubsection}{Local Adaptive Thresholding}{6}{subsubsection*.10}
\contentsline {subsection}{\numberline {2.1.2}Bilaketa Heuristikoa}{6}{subsection.2.1.2}
\contentsline {subsubsection}{Algoritmo Genetikoa}{7}{subsubsection*.12}
\contentsline {section}{\numberline {2.2}Aplikaziorako Softwarea}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Image Magic}{11}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Index Values}{14}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}OpenCV}{15}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Tesseract}{15}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Hiztegiak}{16}{subsection.2.2.5}
\contentsline {subsubsection}{MySpell}{16}{subsubsection*.26}
\contentsline {subsubsection}{Hunspell}{16}{subsubsection*.29}
\contentsline {section}{\numberline {2.3}Oinarrizko Softwarea, Lizentziak eta Instalazioak}{17}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Lizentziak}{18}{subsection.2.3.1}
\contentsline {subsubsection}{Apache lizentzia}{18}{subsubsection*.30}
\contentsline {subsubsection}{BSD lizentzia}{18}{subsubsection*.31}
\contentsline {subsubsection}{GPL lizentzia}{18}{subsubsection*.32}
\contentsline {subsection}{\numberline {2.3.2}Instalazioak}{19}{subsection.2.3.2}
\contentsline {subsubsection}{ImageMagic}{19}{subsubsection*.33}
\contentsline {subsubsection}{OpenCV}{19}{subsubsection*.34}
\contentsline {subsubsection}{Tesseract}{19}{subsubsection*.35}
\contentsline {subsubsection}{Hunspell}{20}{subsubsection*.36}
\contentsline {chapter}{\numberline {3}Diseinua}{21}{chapter.3}
\contentsline {section}{\numberline {3.1}Esperimentazioaren Diseinua}{21}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Ebaluazio-Irizpidea}{22}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Irudien Aurreprozesatzea}{22}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Hiztegiak}{23}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Garapenaren Diseinua}{23}{section.3.2}
\contentsline {chapter}{\numberline {4}Esperimentazioa}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Testu-Irudien Aukeraketa}{25}{section.4.1}
\contentsline {section}{\numberline {4.2}Irudien Aurreprozesaketa}{30}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Trasnformazioen Hautaketa}{30}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Transformazioen Konbinaketa Binaka}{32}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Transformazioen Konbinaketa Bilaketa Heuristikoen Bidez}{34}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Irudi Guztientzako Aurreprozesatze Komuna}{42}{section.4.3}
\contentsline {section}{\numberline {4.4}Hiztegiak}{43}{section.4.4}
\contentsline {chapter}{\numberline {5}Garapena}{45}{chapter.5}
\contentsline {section}{\numberline {5.1}Hasieraketa}{45}{section.5.1}
\contentsline {section}{\numberline {5.2}Bilaketa Heuristikoa}{45}{section.5.2}
\contentsline {section}{\numberline {5.3}OCR}{53}{section.5.3}
\contentsline {section}{\numberline {5.4}Hiztegiak}{54}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Zuzenketa Automatikoa}{54}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Zuzenketa SemiAutomatikoa}{54}{subsection.5.4.2}
\contentsline {chapter}{\numberline {6}Ondorioak eta Etorkizuneko Lanak}{59}{chapter.6}
\contentsline {section}{\numberline {6.1}Ondorioak}{59}{section.6.1}
\contentsline {section}{\numberline {6.2}Etorkizuneko Lanak}{60}{section.6.2}
\contentsline {chapter}{Eranskinak}{}{section*.72}
\contentsline {chapter}{\numberline {A}Aplikazio Nagusia Probatzen}{65}{appendix.A}
\contentsline {section}{\numberline {A.1}1.Adibidea}{66}{section.A.1}
\contentsline {subsection}{\numberline {A.1.1}BilaketaHeuristikoa}{66}{subsection.A.1.1}
\contentsline {subsection}{\numberline {A.1.2}Zuzenketa Automatikoa}{68}{subsection.A.1.2}
\contentsline {section}{\numberline {A.2}2.Adibidea}{68}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}Defektuzko Aurreprozesaketa}{68}{subsection.A.2.1}
\contentsline {subsection}{\numberline {A.2.2}Zuzenketa Semiautomatikoa}{70}{subsection.A.2.2}
\contentsline {chapter}{\numberline {B}Proiektuaren Helburuen Dokumentua}{71}{appendix.B}
\contentsline {section}{\numberline {B.1}Proiektuaren Deskribapena eta Helburuak}{71}{section.B.1}
\contentsline {section}{\numberline {B.2}Atazak}{72}{section.B.2}
\contentsline {section}{\numberline {B.3}Estimazioa eta Egutegia}{75}{section.B.3}
\contentsline {subsection}{\numberline {B.3.1}Estimazioa}{75}{subsection.B.3.1}
\contentsline {subsection}{\numberline {B.3.2}Gantt Diagrama}{75}{subsection.B.3.2}
\contentsline {section}{\numberline {B.4}Irismena}{77}{section.B.4}
\contentsline {section}{\numberline {B.5}Lan Metodologia}{77}{section.B.5}
\contentsline {section}{\numberline {B.6}Arriskuen Analisia}{78}{section.B.6}
\contentsline {section}{\numberline {B.7}Interesatuak}{79}{section.B.7}
\contentsline {chapter}{\numberline {C}Jarraipena eta Konrola}{81}{appendix.C}
\contentsline {section}{\numberline {C.1}Aldaketak}{81}{section.C.1}
\contentsline {section}{\numberline {C.2}Desbideraketak}{81}{section.C.2}
\contentsline {chapter}{Bibliografia}{83}{appendix*.79}
