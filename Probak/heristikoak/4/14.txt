ZERGATIK, ZERTARAKO ETA NOLA MATERIALISMO DIALEKTIKOA:

Hasieran ikusi dugun lez, klase bezala antolatu den burgesiak pentsatzeko
modu bat du, ideologia bat. Bertan, bere helburu politikoak aurrera eramateko
pentsamendu metodo bat garatu du: idealismo metafisikoa. Langileriak,
metodo berri bat sortu du, oinarri ezberdinetatik abiatzen dena, klase bezala
antolatu eta helburu politiko konkretu batzuen anuragarria dena: materialismo dialektikoa.

Lehenak munduaren ikuskera hertsi eta aldebakarrekoa erakusten du, besteak
osoa eta dinamikoa. Batek, burgesiaren dominazioaren alde egiten du, besteak
gizakiaren emantzipazioaren alde. Honela, bi filosofia edo ikuspegi hauek
kontran daude, biak izanik klase filosofia bat, bata edo bestea aukeratzeak
ondorio politikoak izanik, aukeraketa ideologiko bat da.

Azken batean, klase jendarte batean, aukera bat edo beste egiteak,
barrikadaren alde batean edo bestean kokatzen gaitu, bai edo bai. Ez dago
neutrorik, ez dago grisik.

Zergatik? zapalduaren tresna delako materialismo dialektikoa, hau da,
zapalduaren egoeran zergatia azaldu eta honen existentzia politikoa onartuko
du eta, gainera, egoera aldatu daitekeela baieztatu. Zapalduaren mesedetan dagoen filosofia bat da, neutroa ez den filosofia bat, askatasunerantz
garamatzan filosofia. Burgesiak horrenbeste indar egin badu zokoratzeko,
baditugu arrazoiak baliogarria dela pentsatzeko.

Zeratarko? Errealitatea ezagutzeko, ez du eta ezer ezkutatzen uzten, egia du
eta helburu. Multiperspektiba izango den egia, baina egia. Errealitatean eragin
ahal izateko premisa iraultzaileak eskaintzen dizkigu, jendartea, balioak, kulturak
bersortzeko, nahieran. Zapalkuntzarekin amaitzeko, pentsamendu askatzaile bat
sortzeko. Kritikoa izateko gaur egun den metodo eraginkorrena delako.

Nola? Betarrekoetara itzuliz, Materialismo dialektikoa betaurreko batzuk dira.
Edozein gaietara hurbiltzeko materialismo dialektikoaren premisak erabiltzea
izango da betaurreko hauek hanzteko modua. Arazo bat planteatzean gañdetzea,
zein da hone kontrakoa? Zerrekin dago elkarrekintzan? Non izango du eragina?
Zein aldaketatik dator? Zein aldaketa dakar? Oinarri materialista batetik ari gara
edo idealismoaren erortzen ari gara? Galdera hauek, noski, ez dira errazak. Inork ez
du hori ziurtatu.
