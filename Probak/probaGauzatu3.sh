#Eraldaketak burutu
convert -contrast $1 1
convert -contrast 1 concon$1
convert -normalize 1 connor$1
convert -negate 1 conne$1
convert -gaussian-blur 3x3 1 congaus$1
./demo 1 2 M
mv thres.png conM$1

convert -normalize $1 1
convert -contrast 1 norcon$1
convert -normalize 1 nornor$1
convert -negate 1 norne$1
convert -gaussian-blur 3x3 1 norgaus$1
./demo 1 2 M
mv thres.png norM$1

convert -negate $1 1
convert -contrast 1 necon$1
convert -normalize 1 nenor$1
convert -negate 1 nene$1
convert -gaussian-blur 3x3 1 negaus$1
./demo 1 2 M
mv thres.png neM$1

convert -gaussian-blur 3x3 $1 1
convert -contrast 1 gauscon$1
convert -normalize 1 gausnor$1
convert -negate 1 gausne$1
convert -gaussian-blur 3x3 1 gausgaus$1
./demo 1 2 M
mv thres.png gausM$1

./demo $1 2 M
convert -contrast thres.png Mcon$1
convert -normalize thres.png Mnor$1
convert -negate thres.png Mne$1
convert -gaussian-blur 3x3 thres.png Mgaus$1
./demo thres.png 2 M
mv thres.png MM$1





#OCR egin
tesseract -l $2 $1 origi$1

tesseract -l $2 concon$1 concon$1
tesseract -l $2 connor$1 connor$1
tesseract -l $2 conne$1 conne$1
tesseract -l $2 congaus$1 congaus$1
tesseract -l $2 conM$1 conM$1

tesseract -l $2 norcon$1 norcon$1
tesseract -l $2 nornor$1 nornor$1
tesseract -l $2 norne$1 norne$1
tesseract -l $2 norgaus$1 norgaus$1
tesseract -l $2 norM$1 norM$1

tesseract -l $2 necon$1 necon$1
tesseract -l $2 nenor$1 nenor$1
tesseract -l $2 nene$1 nene$1
tesseract -l $2 negaus$1 negaus$1
tesseract -l $2 neM$1 neM$1

tesseract -l $2 gauscon$1 gauscon$1
tesseract -l $2 gausnor$1 gausnor$1
tesseract -l $2 gausne$1 gausne$1
tesseract -l $2 gausgaus$1 gausgaus$1
tesseract -l $2 gausM$1 gausM$1

tesseract -l $2 Mcon$1 Mcon$1
tesseract -l $2 Mnor$1 Mnor$1
tesseract -l $2 Mne$1 Mne$1
tesseract -l $2 Mgaus$1 Mgaus$1
tesseract -l $2 MM$1 MM$1

#konparatu
echo " +++++ "origi$1.txt
python3 konp.py origi$1.txt $3

echo " +++++ "concon$1
python3 konp.py concon$1.txt $3

echo " +++++ "connor$1
python3 konp.py connor$1.txt $3

echo " +++++ "conne$1
python3 konp.py conne$1.txt $3

echo " +++++ "congaus$1
python3 konp.py congaus$1.txt $3

echo " +++++ "conM$1
python3 konp.py conM$1.txt $3

echo " +++++ "norcon$1
python3 konp.py norcon$1.txt $3

echo " +++++ "nornor$1
python3 konp.py nornor$1.txt $3

echo " +++++ "norne$1
python3 konp.py norne$1.txt $3

echo " +++++ "norgaus$1
python3 konp.py norgaus$1.txt $3

echo " +++++ "norM$1
python3 konp.py norM$1.txt $3

echo " +++++ "necon$1
python3 konp.py necon$1.txt $3

echo " +++++ "nenor$1
python3 konp.py nenor$1.txt $3

echo " +++++ "nene$1
python3 konp.py nene$1.txt $3

echo " +++++ "negaus$1
python3 konp.py negaus$1.txt $3

echo " +++++ "neM$1
python3 konp.py neM$1.txt $3

echo " +++++ "gauscon$1
python3 konp.py gauscon$1.txt $3

echo " +++++ "gausnor$1
python3 konp.py gausnor$1.txt $3

echo " +++++ "gausne$1
python3 konp.py gausne$1.txt $3

echo " +++++ "gausgaus$1
python3 konp.py gausgaus$1.txt $3

echo " +++++ "gausM$1
python3 konp.py gausgaus$1.txt $3



echo " +++++ "Mcon$1
python3 konp.py Mcon$1.txt $3

echo " +++++ "Mnor$1
python3 konp.py Mnor$1.txt $3

echo " +++++ "Mne$1
python3 konp.py Mne$1.txt $3

echo " +++++ "Mgaus$1
python3 konp.py Mgaus$1.txt $3

echo " +++++ "MM$1
python3 konp.py MM$1.txt $3

