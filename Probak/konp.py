def testuakKonparatu(izena1, izena2):

	#lehenengo fitxategia irakurri
	fitx1 = open(izena1,'r')
	list1 = []
	for line in fitx1:
		l =  line.split(" ")
		for i in range(0,len(l)):
			string = l[i]
			string = string.replace(",","")
			string = string.replace(".","")
			string = string.replace("\n","")
			if string != "\n":
				list1.append(string)
	fitx1.close()

	#bigarren fitxategia irakurri
	fitx2 = open(izena2,'r')
	list2 = []
	for line in fitx2:
		l =  line.split(" ")
		for i in range(0,len(l)):
			string = l[i]
			string = string.replace(",","")
			string = string.replace(".","")
			string = string.replace("\n","")
			if string != "\n":
				list2.append(string)

	fitx2.close()

	kont = 0
	#Bi listen arteko antzekotasunak begiratu
	for i in range(0,len(list1)):
		 for j in range(0,len(list2)):
		 	if list1[i]!="" and list2[j]!="":
				#print(list1[i].lower()+" == "+list2[j].lower())
		 		if list1[i].lower() == list2[j].lower():
		 		#	print(" ++++ "+list1[i].lower())
		 			kont = kont + 1
		 			list1[i]=""
		 			list2[j]=""
		 			break

	if len(list2)!=0:
		ehunekoa = kont * 100 / len(list2)
	else:
		ehunekoa = 0

	print("**************************************************************")
	print("1 fixategiak bigarrenarekin duen antza => %"+str(ehunekoa))
	print("**************************************************************\n")

	return ehunekoa

import random
from random import shuffle
import sys
import shlex, subprocess
import time
from time import sleep
import operator


testuakKonparatu(sys.argv[1], sys.argv[2])
