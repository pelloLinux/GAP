#Eraldaketak burutu
convert -contrast $1 contrast$1
convert -normalize $1 normalize$1
convert -negate $1 negate$1
convert -edge 2 $1 edge2$1
convert -edge 4 $1 edge4$1
convert -lat 3x3 $1 lat$1
convert -gaussian-blur 3x3 $1 gaussian$1
./demo $1 1 M
mv thres.png moran1$1.png
./demo $1 3 M
mv thres.png moran3$1.png
./demo $1 1 E1
mv thres.png edu1$1.png
./demo $1 3 E1
mv thres.png edu3$1.png

#OCR egin
tesseract -l $2 $1 origi$1
tesseract -l $2 contrast$1 contrast$1
tesseract -l $2 normalize$1 normalize$1
tesseract -l $2 negate$1 negate$1
tesseract -l $2 edge2$1 edge2$1
tesseract -l $2 edge4$1 edge4$1
tesseract -l $2 lat$1 lat$1
tesseract -l $2 gaussian$1 gaussian$1

tesseract -l $2 moran1$1.png moran1$1
tesseract -l $2 moran3$1.png moran3$1
tesseract -l $2 edu1$1.png edu1$1
tesseract -l $2 edu3$1.png edu3$1

#konparatu
echo " +++++ "origi$1.txt
python3 konp.py origi$1.txt $3
echo " +++++ "contrast$1
python3 konp.py contrast$1.txt $3
echo " +++++ "normalize$1
python3 konp.py normalize$1.txt $3
echo " +++++ "negate$1
python3 konp.py negate$1.txt $3
echo " +++++ "edge2$1
python3 konp.py edge2$1.txt $3
echo " +++++ "edge4$1
python3 konp.py edge4$1.txt $3
echo " +++++ "lat$1
python3 konp.py lat$1.txt $3
echo " +++++ "gaussian$1
python3 konp.py gaussian$1.txt $3
echo " +++++ "moran1$1
python3 konp.py moran1$1.txt $3
echo " +++++ "moran3$1
python3 konp.py moran3$1.txt $3
echo " +++++ "edu1$1
python3 konp.py edu1$1.txt $3
echo " +++++ "edu3$1
python3 konp.py edu3$1.txt $3

